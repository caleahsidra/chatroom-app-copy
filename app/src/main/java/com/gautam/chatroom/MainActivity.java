/**
 *
 */
package com.gautam.chatroom;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserInfo;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    public static final String ANONYMOUS = "anonymous";
    public static final int DEFAULT_MSG_LENGTH_LIMIT = 1000;
    public static final int RC_SIGN_IN = 1;
    static final int REQUEST_IMAGE_GET = 1;
    private static final int RC_PHOTO_PICKER =  2;

    private ListView mMessageListView;
    private ImageButton mPhotoSelector;
    private EditText newMessageView;
    private Button mSendButton;

    private String userName;
    private MyAdapter myAdapter;
    List<MessageType> messages = new ArrayList<>();
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yy K:mm a", Locale.US);

    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseRef;
    private FirebaseStorage firebaseStorage;
    private StorageReference storageRef;
    private ChildEventListener mChildEventListener;
    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener authStateListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        userName = ANONYMOUS;

        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseRef = firebaseDatabase.getReference().child("messages");

        firebaseStorage = FirebaseStorage.getInstance();
        storageRef = firebaseStorage.getReference().child("chat_photos");

        firebaseAuth = FirebaseAuth.getInstance();

        // Initialize references to views
        mMessageListView = (ListView) findViewById(R.id.msgListView);
        mPhotoSelector = (ImageButton) findViewById(R.id.photoSelector);
        newMessageView = (EditText) findViewById(R.id.newMessageView);
        mSendButton = (Button) findViewById(R.id.sendButton);

        // Initialize message ListView and its adapter
        myAdapter = new MyAdapter(this, R.layout.message, messages);
        mMessageListView.setAdapter(myAdapter);

        // ImagePickerButton shows an image picker to upload a image for a message
        mPhotoSelector.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // TODO: Fire an intent to show an image picker

                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
                intent.setType("image/*");
                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(Intent.createChooser(intent, "Complete action using"),RC_PHOTO_PICKER);
                }
            }
        });

        // Enable Send button when there's text to send
        newMessageView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().trim().length() > 0) {
                    mSendButton.setEnabled(true);
                } else {
                    mSendButton.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
        newMessageView.setFilters(new InputFilter[]{new InputFilter.LengthFilter(DEFAULT_MSG_LENGTH_LIMIT)});

        // Send button sends a message and clears the EditText
        mSendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String date = dateFormat.format(new Date());
                MessageType message = new MessageType(newMessageView.getText().toString(), userName, null, date);
                databaseRef.push().setValue(message);

                newMessageView.setText("");
            }
        });


        authStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();

                if(user != null) {
                    String userName = user.getDisplayName();

                    for (UserInfo userInfo : user.getProviderData()) {
                        if (userName == null && userInfo.getDisplayName() != null) {
                            userName = userInfo.getDisplayName();
                        }
                    }

                    if (userName==null) {
                        Toast.makeText(MainActivity.this, "Sign in cancelled", Toast.LENGTH_SHORT).show();
                        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                        builder.setTitle("Login again!!")
                                .setMessage("Due to some reason we are not able to idetify you\nPlease login again!!")
                                .setCancelable(false)
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        //do things
                                    }
                                });
                        builder.create().show();

                        onSignOutAuth();
                        startActivityForResult(
                                AuthUI.getInstance()
                                        .createSignInIntentBuilder()
                                        .setIsSmartLockEnabled(false)
                                        .setProviders(Arrays.asList(new AuthUI.IdpConfig.Builder(AuthUI.EMAIL_PROVIDER).build(),
                                                new AuthUI.IdpConfig.Builder(AuthUI.GOOGLE_PROVIDER).build()))
                                        .build(),
                                RC_SIGN_IN);
                    }
                    else  onSignInAuth(userName);

                } else {
                    onSignOutAuth();
                    startActivityForResult(
                            AuthUI.getInstance()
                                    .createSignInIntentBuilder()
                                    .setIsSmartLockEnabled(false)
                                    .setProviders(Arrays.asList(new AuthUI.IdpConfig.Builder(AuthUI.EMAIL_PROVIDER).build(),
                                            new AuthUI.IdpConfig.Builder(AuthUI.GOOGLE_PROVIDER).build()))
                                    .build(),
                            RC_SIGN_IN);
                }
            }
        };
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == RC_SIGN_IN) {
            if (resultCode == RESULT_OK)
                Toast.makeText(MainActivity.this, "Signed in successfully", Toast.LENGTH_SHORT).show();
            else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(MainActivity.this, "Sign in cancelled", Toast.LENGTH_SHORT).show();
                finish();
            }
        } else  if(requestCode == RC_PHOTO_PICKER && resultCode == RESULT_OK){
            Uri selectedImageUri = data.getData();
            StorageReference photoRef = storageRef.child(selectedImageUri.getLastPathSegment());

            photoRef.putFile(selectedImageUri).addOnSuccessListener(this, new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    @SuppressWarnings("VisibleForTests") Uri downloadUrl = taskSnapshot.getDownloadUrl();
                    assert downloadUrl != null;
                    String date = dateFormat.format(new Date());
                    MessageType message = new MessageType(null, userName, downloadUrl.toString(), date);
                    databaseRef.push().setValue(message);
                }
            });
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if(authStateListener != null) {
            firebaseAuth.removeAuthStateListener(authStateListener);
        }
        myAdapter.clear();

        if(mChildEventListener != null) {
            databaseRef.removeEventListener(mChildEventListener);
            mChildEventListener = null;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        firebaseAuth.addAuthStateListener(authStateListener);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.sign_out_menu){
            AuthUI.getInstance().signOut(this);
            return  true;
        }
        else return super.onOptionsItemSelected(item);
    }

    private void onSignInAuth( String userName){
        this.userName = userName;
        addEventListener();
    }

    private void onSignOutAuth(){
        userName = ANONYMOUS;
        myAdapter.clear();
        if(mChildEventListener != null) {
            databaseRef.removeEventListener(mChildEventListener);
            mChildEventListener = null;
        }

    }

    /*public void sendImage()
    {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("mage/jpeg");
        intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        startActivityForResult(Intent.createChooser(intent ,"Complete action using "),RC_PhotoPicker);

    }*/

    private void addEventListener() {
        if(mChildEventListener == null) {

            mChildEventListener = new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    MessageType message = dataSnapshot.getValue(MessageType.class);
                    myAdapter.add(message);
                    myAdapter.notifyDataSetChanged();
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {
                    MessageType message = dataSnapshot.getValue(MessageType.class);

                    int i;
                    String name = message.getName();
                    String txt = message.getText();
                    String photoUrl = message.getPhotoUrl();
                    String date = message.getDate();
                    for (i=0; i<myAdapter.getCount(); i++) {
                        MessageType m = myAdapter.getItem(i);
                        if (name==m.getName() && txt==m.getText() && photoUrl==m.getPhotoUrl() && date==m.getDate()) {
                            break;
                        }
                    }
                    myAdapter.remove(myAdapter.getItem(i));
                    myAdapter.notifyDataSetChanged();
                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            };
            databaseRef.addChildEventListener(mChildEventListener);
        }
    }
}
