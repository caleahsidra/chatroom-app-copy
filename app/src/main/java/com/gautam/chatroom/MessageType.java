package com.gautam.chatroom;

/**
 * Created by gautam on 11/7/2017.
 *
 * message class for transfer and showing messages
 */

public class MessageType {

    private String text;
    private String name;
    private String photoUrl;
    private String date;

    public MessageType() {
    }

    public MessageType(String text, String name, String photoUrl, String date) {
        this.text = text;
        this.name = name;
        this.photoUrl = photoUrl;
        this.date = date;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getDate() {
        return this.date;
    }

    public void setDate(String date) {
        this.date = date;
    }

}
